"use strict";

class m3u {

  constructor(txt) {
    this.txt = txt;
    this.textArr = this.txt.split('#EXTINF').splice(1);
    this.result = [];
  }


  create() {

    this.textArr.forEach(item => {

      let reg = /:(\d+),\s(.+)\s-\s(.+)\n(.+)/g;
      let parsed = reg.exec(item);

      if (parsed !== null) {
        this.result.push({
          length: parsed[1],
          author: parsed[2],
          title: parsed[3],
          path: parsed[4]
        });
      } else {
        console.error('File doesn\'t valid');
        console.error('Record is crashed');
      }
    });

    return this.result;
  };

  parse() {

    if (!this.hasHeader()) {
      console.error('File hasn\'t header \'#EXTM3U\'');
      return
    }

    return this.create()
  };

  hasHeader() {
    let header = this.txt.indexOf('#EXTM3U');
    return header >= 0;
  };
}

module.exports = m3u;