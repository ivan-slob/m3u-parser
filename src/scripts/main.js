"use strict";

let m3u = require('./m3u');

let ready = function (fn) {
  if (typeof fn !== 'function') return;
  if (document.readyState === 'complete') {
    return fn();
  }
  document.addEventListener('DOMContentLoaded', fn, false);
};


ready(function () {
  let resultElement = document.getElementById('result'),
    result,
    reader;

  function handleFile(event) {
    reader = new FileReader();

    reader.addEventListener('loadend', function () {
      document.getElementById('upload').innerText = reader.result;

      let _m3u = new m3u(reader.result);

      result = _m3u.parse();

      resultElement.innerText = JSON.stringify(result, undefined, 2);

    });

    reader.readAsText(event.target.files[0])
  }

  document.getElementById('files').addEventListener('change', handleFile, false);
});